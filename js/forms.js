$(document).ready(function() {
	//FORMS
	function forms(){
		$('input,textarea').focus(function(){
			if($(this).val() == $(this).attr('data-value')){
					$(this).addClass('focus');
					$(this).parent().addClass('focus');
					$(this).removeClass('err');
					$(this).parent().removeClass('err');
				if($(this).attr('data-type')=='pass'){
					$(this).attr('type','password');
				};
				$(this).val('');
			};
		});
		$('input[data-value], textarea[data-value]').each(function() {
			if (this.value == '' || this.value == $(this).attr('data-value')) {
				this.value = $(this).attr('data-value');
			}
			$(this).click(function() {
				if (this.value == $(this).attr('data-value')) {
					if($(this).attr('data-type')=='pass'){
						$(this).attr('type','password');
					};
					this.value = '';
				};
			});
			$(this).blur(function() {
				if (this.value == '') {
					this.value = $(this).attr('data-value');
						$(this).removeClass('focus');
						$(this).parent().removeClass('focus');
					if($(this).attr('data-type')=='pass'){
						$(this).attr('type','text');
					};
				};
			});
		});
	}
	forms();

});